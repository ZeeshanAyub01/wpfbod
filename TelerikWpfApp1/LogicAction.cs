﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace TelerikWpfApp1
{
    class LogicAction
    {
        public LogicAction(string name)
        {
            this.Name = name;
            this.ActionsList = new ObservableCollection<Action>();
        }
        public string Name
        {
            get;
            set;
        }
        public ObservableCollection<Action> ActionsList
        {
            get;
            set;
        }
    }
}
