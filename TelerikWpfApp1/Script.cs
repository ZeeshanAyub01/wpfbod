﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace TelerikWpfApp1
{
    class Script
    {
        public Script(string name)
        {
            this.Name = name;
            this.ActionsList = new ObservableCollection<Object>();
        }
        public string Name
        {
            get;
            set;
        }
        public ObservableCollection<Object> ActionsList
        {
            get;
            set;
        }
    }
}
