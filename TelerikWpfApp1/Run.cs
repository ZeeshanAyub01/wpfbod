﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TelerikWpfApp1
{
    public class Run : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string name;
        private DateTime dueDate;
        private int orderNumber;

        public string Name
        {
            get { return this.name; }
            set
            {
                if (value != this.name)
                {
                    this.name = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        public DateTime DueDate
        {
            get { return this.dueDate; }
            set
            {
                if (value != this.dueDate)
                {
                    this.dueDate = value;
                    this.OnPropertyChanged("DueDate");
                }
            }
        }

        public int OrderNumber
        {
            get { return this.orderNumber; }
            set
            {
                if (value != this.orderNumber)
                {
                    this.orderNumber = value;
                    this.OnPropertyChanged("OrderNumber");
                }
            }
        }

        public Run(string name, DateTime dueDate, int orderNumber)
        {
            this.name = name;
            this.dueDate = dueDate;
            this.orderNumber = orderNumber;
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
    }
}
