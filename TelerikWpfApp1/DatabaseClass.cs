﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace TelerikWpfApp1
{
    class DatabaseClass
    {
        //public static void Main()
        //{
        //    DataTable table = loadFromDatabase();
        //    foreach (DataRow row in table.Rows)
        //    {
        //        foreach (object item in row.ItemArray)
        //        {
        //            Console.Write($"{item.ToString()}\t");
        //        }
        //        Console.WriteLine();
        //    }
        //    Console.ReadLine();
        //}
        public static DataTable loadFromDatabase()
        {
            SQLiteConnection connection = new SQLiteConnection($"DataSource=C:/Users/mza/Documents/Mantech/WPFBOD_Database/wpfbod.sqlite;Version=3;");
            connection.Open();
            SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM 'TempRunTable'", connection);
            SQLiteDataReader reader = cmd.ExecuteReader();
            DataTable table = new DataTable();
            table.Load(reader);
            return table;
        }
        public static void insert(string runName, int orderNumber)
        {
            string qCommand = String.Format("INSERT INTO TempRunTable VALUES(\'{0}\', {1})", runName, orderNumber);
            SQLiteConnection connection = new SQLiteConnection($"DataSource=./database.db;Version=3;");
            connection.Open();
            SQLiteCommand cmd = new SQLiteCommand(qCommand, connection);
            cmd.ExecuteNonQuery();
        }
        public static void createDatabase()
        {
            SQLiteConnection connection = new SQLiteConnection($"DataSource=./database.db;Version=3;");
            connection.Open();
            SQLiteCommand cmd = new SQLiteCommand("CREATE TABLE IF NOT EXISTS \"TempRunTable\" (`Runs` PRIMARY KEY, `OrderNumber` INTEGER DEFAULT 0)", connection);
            cmd.ExecuteNonQuery();
        }
    }
}
