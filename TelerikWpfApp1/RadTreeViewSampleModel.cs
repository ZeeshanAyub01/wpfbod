﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace TelerikWpfApp1
{
    class RadTreeViewSampleModel
    {
        public RadTreeViewSampleModel()
        {
            this.InitializeScriptDataSource();
        }
        public ObservableCollection<Script> ScriptsDataSource
        {
            get;
            set;
        }
        private void InitializeScriptDataSource()
        {
            this.ScriptsDataSource = new ObservableCollection<Script>();
            Script s;
            LogicAction l;
            Action a;
            this.ScriptsDataSource.Add(s = new Script("BOD Script"));
            s.ActionsList.Add((l = new LogicAction("if (some case)")));
            l.ActionsList.Add(new Action("Wait Action"));
            l.ActionsList.Add(new Action("Stirring Action"));
            l.ActionsList.Add(new Action("Burret Action"));
            s.ActionsList.Add((a = new Action("Wait Action")));
            s.ActionsList.Add((a = new Action("Report Action")));
            s.ActionsList.Add((l = new LogicAction("while (some case)")));
            l.ActionsList.Add(new Action("Burette Action"));
            l.ActionsList.Add(new Action("Stirring Action"));
            l.ActionsList.Add(new Action("Random Action"));
            s.ActionsList.Add((a = new Action("Wait Action")));
            s.ActionsList.Add((a = new Action("Report Action")));
        }
    }
}
