﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Telerik.Windows.Controls;

namespace TelerikWpfApp1
{
    public class HomeTableViewModel : ViewModelBase
    {
        private ObservableCollection<Run> runs;

        public ObservableCollection<Run> Runs
        {
            get
            {
                if (this.runs == null)
                {
                    this.runs = this.CreateRuns();
                }

                return this.runs;
            }
        }

        private ObservableCollection<Run> CreateRuns()
        {
            ObservableCollection<Run> runs = new ObservableCollection<Run>();
            Run run;

            run = new Run("Run One", new DateTime(2020, 12, 13), 131989);
            runs.Add(run);

            run = new Run("Run Two", new DateTime(2020, 12, 14), 131990);
            runs.Add(run);

            run = new Run("Run Three", new DateTime(2020, 12, 15), 131991);
            runs.Add(run);

            return runs;
        }
    }

}
