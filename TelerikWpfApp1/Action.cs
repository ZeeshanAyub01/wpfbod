﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelerikWpfApp1
{
    class Action
    {
        public Action(string name)
        {
            this.Name = name;
        }
        public string Name
        {
            get;
            set;
        }
    }
}
