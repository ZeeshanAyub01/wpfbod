﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace TelerikWpfApp1
{
    class Rack
    {
        public String RackName
        {
            get;
            set;
        }
        public String IconPath
        {
            get;
            set;
        }

        public static ObservableCollection<Rack> GenerateRacks()
        {
            ObservableCollection<Rack> result = new ObservableCollection<Rack>();
            Rack rack1 = new Rack();
            rack1.RackName = "Rack1";
            rack1.IconPath = "pack://application:,,,/Images/one.jpg";
            result.Add(rack1);
            Rack rack2 = new Rack();
            rack2.RackName = "Rack2";
            rack2.IconPath = "pack://application:,,,/Images/two.jpg";
            result.Add(rack2);
            Rack rack3 = new Rack();
            rack3.RackName = "Rack 3";
            rack3.IconPath = "pack://application:,,,/Images/three.jpg";
            result.Add(rack3);
            Rack rack4 = new Rack();
            rack4.RackName = "Rack 4";
            rack4.IconPath = "pack://application:,,,/Images/four.jpg";
            result.Add(rack4);
            Rack rack5 = new Rack();
            rack5.RackName = "Rack 5";
            rack5.IconPath = "pack://application:,,,/Images/five.jpg";
            result.Add(rack5);
            return result;
        }
    }
}
