﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Controls;
using TelerikWpfApp1.UserControls;

namespace TelerikWpfApp1
{
    public class NavigationItemModel
    {
        public string Title { get; set; }
        public string IconGlyph { get; set; }
        public ObservableCollection<NavigationItemModel> Children { get; set; }

        public UserControl Control { get; set; }

    }

    public class MainViewModel
    {
        //private readonly string[] glyphStrings = new string[] { "&#xe679;", "&#xe516;", "&#xe13b;", "&#xe64b;", "&#xe13c;", "&#xe665;", "&#xe10e;", "&#xe13e;", "&#xe401;" };
        //private readonly string[] menuItemStrings = new string[] { "Home", "BOD", "Setup", "Quality Control", "Hardware", "Reporting", "Utilities", "Audit Trail", "About" };
        //private UserControl[] controlsList = new UserControl[] { new UserControlHome(), new UserControlBOD(), new UserControlSetup(), new UserControlSetup(), new UserControlSetup(), new UserControlSetup(), new UserControlSetup(), new UserControlSetup(), new UserControlSetup() };

        public ObservableCollection<NavigationItemModel> Items { get; set; }


        public MainViewModel()
        {
            this.Items = new ObservableCollection<NavigationItemModel>();

            //for (int i = 1; i <= 9; i++)
            //{
            //    var glyphString = this.glyphStrings[i - 1];
            //    var menuName = this.menuItemStrings[i - 1];
            //    var screenControl = this.controlsList[i - 1];
            //    this.Items.Add(new NavigationItemModel() { Title = menuName, IconGlyph = glyphString, Control = screenControl });
            //}

            // UserControl controlObject = new UserControlHome();

            //HOME
            this.Items.Add(
                new NavigationItemModel()
                {
                    Title = "Home",
                    IconGlyph = "&#xe679;",
                    Control = new UserControlHome()
                }); 
            
            // BOD
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "BOD",
                     IconGlyph = "&#xe516;",
                     Control = new UserControlBOD(),
                     Children = new ObservableCollection<NavigationItemModel>()
                        {
                            new NavigationItemModel() { Title = "Run BOD", IconGlyph = "&#xe801;", Control =  new UserControlBODRun()},
                            new NavigationItemModel() { Title = "Post Run BOD Analysis", IconGlyph = "&#xe801;", Control =  new BOD_PostRunBODAnalysis() },
                            new NavigationItemModel() { Title = "Manual Control", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Manual Edit", IconGlyph = "&#xe801;" },                            
                            new NavigationItemModel() { Title = "Manual Calibration", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Examine Calibration", IconGlyph = "&#xe801;" },

                        }
                 });

            // Setup
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "Setup",
                     IconGlyph = "&#xe13b;",
                     Control = new UserControlSetup(),
                     Children = new ObservableCollection<NavigationItemModel>()
                        {
                            new NavigationItemModel() { Title = "Analysis Schedule", IconGlyph = "&#xe801;", Control =  new Setup_AnalysisSchedule()},
                            new NavigationItemModel() { Title = "BOD Method", IconGlyph = "&#xe801;",  Control =  new Setup_BODMethod()},
                            new NavigationItemModel() { Title = "Edit Timetable Template", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Formula Definition", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Calibration Template", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Trays Template", IconGlyph = "&#xe801;" ,  Control =  new Setup_TraysTemplate() },

                        }
                 });

            // Quality Control
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "Quality Control",
                     IconGlyph = "&#xe64b;",
                     Control = new UserControlQC(),
                     Children = new ObservableCollection<NavigationItemModel>()
                        {
                            new NavigationItemModel() { Title = "View A QC Plot", IconGlyph = "&#xe801;", Control =  new UserControlSetup()},
                            new NavigationItemModel() { Title = "Setup A QC Regime", IconGlyph = "&#xe801;" },
                        }
                 });

            // Hardware
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "Hardware",
                     IconGlyph = "&#xe13c;",
                     Control = new UserControlHardware(),
                     Children = new ObservableCollection<NavigationItemModel>()
                        {
                            new NavigationItemModel() { Title = "Hardware Setup", IconGlyph = "&#xe801;", Control =  new UserControlSetup()},
                        }
                 });

            //  Reporting 
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "Reporting",
                     IconGlyph = "&#xe665;",
                     Control = new UserControlReporting(),
                     Children = new ObservableCollection<NavigationItemModel>()
                        {
                            new NavigationItemModel() { Title = "Create/Edit A Report List", IconGlyph = "&#xe801;", Control =  new UserControlSetup()},
                            new NavigationItemModel() { Title = "Print A Report", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Prepare/Print Shazam Reports", IconGlyph = "&#xe801;" },
                        }
                 });

            //  Utilities 
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "Utilities",
                     IconGlyph = "&#xe10e5;",
                     Control = new UserControlUtil(),
                     Children = new ObservableCollection<NavigationItemModel>()
                        {
                            new NavigationItemModel() { Title = "Edit Autorun Buttons", IconGlyph = "&#xe801;", Control =  new UserControlSetup()},
                            new NavigationItemModel() { Title = "Passwords", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Utility Databases", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Database Zipping", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Options", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Pack/Repair Databases", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Set Columns For Display", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Edit Aliases", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Edit Headers", IconGlyph = "&#xe801;" },
                            new NavigationItemModel() { Title = "Archive Databases", IconGlyph = "&#xe801;" },

                        }
                 }); ;

            //  Audit Trail 
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "Audit Trail",
                     IconGlyph = "&#xe13e;",
                     Control = new UserControlAuditTrail(),
                     Children = new ObservableCollection<NavigationItemModel>()
                        {
                            new NavigationItemModel() { Title = "View Changes", IconGlyph = "&#xe801;", Control =  new UserControlSetup()},
                            new NavigationItemModel() { Title = "View Audit Trail", IconGlyph = "&#xe801;" },

                        }
                 });

            //About 
            this.Items.Add(
                 new NavigationItemModel()
                 {
                     Title = "About",
                     IconGlyph = "&#xe401;",
                     Control = new UserControlAbout(),
                 });
        }
    }
}
