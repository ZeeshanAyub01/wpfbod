﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace TelerikWpfApp1
{
    /// <summary>
    /// Interaction logic for UserControlSetup.xaml
    /// </summary>
    public partial class UserControlSetup : UserControl
    {
        public UserControlSetup()
        {
            InitializeComponent();
        }

        private void btn_logoPop_Click(object sender, RoutedEventArgs e)
        {
            RadWindow.Alert(new DialogParameters()
            {
                Header = "Mantech BOD Popup",
                Content = "This is a special Mantech window!",
                IconTemplate = this.Resources["IconTemplate"] as DataTemplate
            });

        }
    }
}
