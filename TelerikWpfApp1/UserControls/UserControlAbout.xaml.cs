﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TelerikWpfApp1.UserControls
{
    /// <summary>
    /// Interaction logic for UserControlAbout.xaml
    /// </summary>
    public partial class UserControlAbout : UserControl
    {
        public UserControlAbout()
        {
            InitializeComponent();
        }

        private void btn_clickMe_Click(object sender, RoutedEventArgs e)
        {
            //STANDARD WPF MESSAGEBOX
            MessageBoxResult result = MessageBox.Show("Do you want to close this window?",
                                                      "Confirmation",
                                                      MessageBoxButton.YesNo,
                                                      MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }

        }
    }
}
