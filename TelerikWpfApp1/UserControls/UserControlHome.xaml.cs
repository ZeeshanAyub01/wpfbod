﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Sparklines;

namespace TelerikWpfApp1.UserControls
{
    /// <summary>
    /// Interaction logic for UserControlHome.xaml
    /// </summary>
    public partial class UserControlHome : UserControl
    {
        DataTable table = new DataTable();

        public UserControlHome()
        {
            Console.WriteLine("!!!--->>> Trying to make contact with the database");
            InitializeComponent();
            LoadTableFromDB();
            rgv_homeRunTable.DataContext = table.DefaultView;
        }

        void LoadTableFromDB()
        {
            SQLiteConnection connection = new SQLiteConnection("URI=file:./database.db");
            
            try
            {
                SQLiteCommand cmd = new SQLiteCommand();
                connection.Open();
                //SQLiteDataReader reader = cmd.ExecuteReader();
                //table.Load(reader);
                cmd.CommandText = "SELECT * FROM TempRunTable";
                cmd.Connection = connection;
                SQLiteDataReader reader = cmd.ExecuteReader();
                table.Load(reader);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Couldn't connect to the database " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void btn_addToTable_Click(object sender, RoutedEventArgs e)
        {
            //show messagebox to confirm if want to add
            ShowConfirm();
        }

        public void ShowConfirm()
        {
            RadWindow.Confirm("Are you sure you want to add this entry?", this.OnClosed);
        }
        private void OnClosed(object sender, WindowClosedEventArgs e)
        {
            var result = e.DialogResult;
            if (result == true)
            {
                // handle confirmation 
                //add entry to db
                string runName = tb_RunName.Text;
                string order = tb_OrderNumber.Text;
                int orderNum = Convert.ToInt32(order);
                DatabaseClass.insert(runName, orderNum);

                LoadTableFromDB();
                rgv_homeRunTable.DataContext = table.DefaultView;
            }
        }

    }
}
