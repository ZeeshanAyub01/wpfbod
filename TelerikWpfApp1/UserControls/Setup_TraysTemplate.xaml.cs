﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.DragDrop;

namespace TelerikWpfApp1.UserControls
{
    /// <summary>
    /// Interaction logic for Setup_TraysTemplate.xaml
    /// </summary>
    public partial class Setup_TraysTemplate : UserControl
    {
        public Setup_TraysTemplate()
        {
            InitializeComponent();
            TraysList1.ItemsSource = Rack.GenerateRacks();
            TraysList2.ItemsSource = new ObservableCollection<Rack>();

            DragDropManager.AddDragInitializeHandler(TraysList1, OnDragInitialize);
            DragDropManager.AddDragInitializeHandler(TraysList2, OnDragInitialize);

            DragDropManager.AddGiveFeedbackHandler(TraysList1, OnGiveFeedback);
            DragDropManager.AddGiveFeedbackHandler(TraysList2, OnGiveFeedback);

            DragDropManager.AddDragDropCompletedHandler(TraysList1, OnDragCompleted);
            DragDropManager.AddDragDropCompletedHandler(TraysList2, OnDragCompleted);

            DragDropManager.AddDropHandler(TraysList1, OnDrop);
            DragDropManager.AddDropHandler(TraysList2, OnDrop);

        }

        private void OnDragInitialize(object sender, DragInitializeEventArgs args)
        {
            args.AllowedEffects = DragDropEffects.All;
            var payload = DragDropPayloadManager.GeneratePayload(null);
            var data = ((FrameworkElement)args.OriginalSource).DataContext;
            payload.SetData("DragData", data);
            args.Data = payload;
            args.DragVisual = new ContentControl { Content = data, ContentTemplate = LayoutRoot.Resources["TraysListTemplate"] as DataTemplate };
        }

        private void OnGiveFeedback(object sender, Telerik.Windows.DragDrop.GiveFeedbackEventArgs args)
        {
            args.SetCursor(Cursors.Arrow);
            args.Handled = true;
        }

        private void OnDrop(object sender, Telerik.Windows.DragDrop.DragEventArgs args)
        {
            var data = ((DataObject)args.Data).GetData("DragData");
            ((IList)(sender as ListBox).ItemsSource).Add(data);
        }

        public void OnDragCompleted(object sender, Telerik.Windows.DragDrop.DragDropCompletedEventArgs args)
        {
            var data = DragDropPayloadManager.GetDataFromObject(args.Data, "DragData");
            ((IList)(sender as ListBox).ItemsSource).Remove(data);
        }

   
    }
}
