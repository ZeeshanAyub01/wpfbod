﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TelerikWpfApp1.UserControls
{
    /// <summary>
    /// Interaction logic for BOD_PostRunBODAnalysis.xaml
    /// </summary>
    public partial class BOD_PostRunBODAnalysis : UserControl
    {
        SQLiteConnection connection = new SQLiteConnection("URI=file:./wpfbod.sqlite");
        DataTable tbl_Sequences = new DataTable();

        public BOD_PostRunBODAnalysis()
        {
            InitializeComponent();
        }

        private void btn_SelectMethod_Click(object sender, RoutedEventArgs e)
        {
            SQLiteCommand cmd;
            SQLiteDataReader reader;

            try
            {
                cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM Sequences";
                connection.Open();
                
                reader = cmd.ExecuteReader();
                tbl_Sequences.Load(reader);
                //MessageBox.Show("dfsfsdf" + tbl_Sequences.Rows.Count + " and " + tbl_Sequences.Columns.Count);
                //foreach(DataColumn col in tbl_Sequences.Columns)
                //{

                //    rgv_Sequences.Columns.Add(col.ColumnName);
                //}
                rgv_Sequences.ItemsSource = tbl_Sequences;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't connect to the database " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void rgv_Sequences_SelectedCellsChanged(object sender, Telerik.Windows.Controls.GridView.GridViewSelectedCellsChangedEventArgs e)
        {
            MessageBox.Show("A new row selected");
        }
    }
}
